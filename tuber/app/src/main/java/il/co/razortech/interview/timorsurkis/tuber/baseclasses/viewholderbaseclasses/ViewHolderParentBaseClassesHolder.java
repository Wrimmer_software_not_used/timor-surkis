package il.co.razortech.interview.timorsurkis.tuber.baseclasses.viewholderbaseclasses;

import java.util.ArrayList;
import java.util.List;

import il.co.razortech.interview.timorsurkis.tuber.baseclasses.data.PlaylistsContainer;
import il.co.razortech.interview.timorsurkis.tuber.baseclasses.data.VideoList;

/**
 * Created by Wrimmer on 01/02/2016.
 */
public class ViewHolderParentBaseClassesHolder {
    private List<ViewHolderParentBaseClass> parents;

    public ViewHolderParentBaseClassesHolder(PlaylistsContainer playlistsContainer) {
        parents = new ArrayList<>();
        for (VideoList videoList : playlistsContainer.getListsOfVideoList()) {
            parents.add(new ViewHolderParentBaseClass(videoList.getListTitle(), videoList.getListItems()));
        }
    }

    public List<ViewHolderParentBaseClass> getParents() {
        return parents;
    }
}
