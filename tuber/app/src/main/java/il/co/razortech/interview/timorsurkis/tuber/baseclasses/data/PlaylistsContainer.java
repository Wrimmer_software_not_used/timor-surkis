package il.co.razortech.interview.timorsurkis.tuber.baseclasses.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Wrimmer on 31/01/2016.
 */
public class PlaylistsContainer implements Serializable {
    @SerializedName("Playlists") private List<VideoList> playlists;

    public PlaylistsContainer() {
    }

    public PlaylistsContainer(List<VideoList> listsOfVideoList) {
        this.playlists = listsOfVideoList;
    }

    public List<VideoList> getListsOfVideoList() {
        return playlists;
    }

    public void setListsOfVideoList(List<VideoList> listsOfVideoList) {
        this.playlists = listsOfVideoList;
    }
}
