package il.co.razortech.interview.timorsurkis.tuber.baseclasses.data;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Wrimmer on 31/01/2016.
 */
public class VideoList implements Serializable, ParentListItem{
    @SerializedName("ListTitle") private String listTitle;
    @SerializedName("ListItems") private List<ListItems> listItems;

    public String getListTitle() {
        return listTitle;
    }

    public void setListTitle(String listTitle) {
        this.listTitle = listTitle;
    }

    public List<ListItems> getListItems() {
        return listItems;
    }

    public void setListItems(List<ListItems> listItems) {
        this.listItems = listItems;
    }

    @Override
    public List<?> getChildItemList() {
        return listItems;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
