package il.co.razortech.interview.timorsurkis.tuber.baseclasses.viewholderbaseclasses;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.ArrayList;
import java.util.List;

import il.co.razortech.interview.timorsurkis.tuber.baseclasses.data.ListItems;

/**
 * Created by Wrimmer on 01/02/2016.
 */
public class ViewHolderParentBaseClass implements ParentListItem {
    private String title;
    private ViewHolderChidBaseClass child;

    public ViewHolderParentBaseClass(String title, List<ListItems> listOfItems) {
        this.title = title;
        this.child = new ViewHolderChidBaseClass(listOfItems);
    }

    public String getTitle() {
        return title;
    }

    @Override
    public List<?> getChildItemList() {
        List<ViewHolderChidBaseClass> childs = new ArrayList<>();
        childs.add(child);
        return childs;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
