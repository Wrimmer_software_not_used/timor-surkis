package il.co.razortech.interview.timorsurkis.tuber.baseclasses.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Wrimmer on 31/01/2016.
 */
public class ListItems implements Serializable {
    @SerializedName("Title") private String title;
    private String link;
    private String thumb;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }
}
