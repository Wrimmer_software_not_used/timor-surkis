package il.co.razortech.interview.timorsurkis.tuber;

import android.util.Log;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import il.co.razortech.interview.timorsurkis.tuber.baseclasses.data.PlaylistsContainer;

/**
 * Created by Wrimmer on 31/01/2016.
 */
public class YoutubeVideosJsonParser {
    public static PlaylistsContainer downlodListOfYoutubeVideos() {
        Gson gson = new Gson();
        BufferedReader reader = null;
        PlaylistsContainer playlistsContainer = null;
        try {
            URL url = new URL("http://www.razor-tech.co.il/hiring/youtube-api.json");

            reader = new BufferedReader(new InputStreamReader(url.openStream()));
//            StringBuffer buffer = new StringBuffer();
//            int read;
//            char [] chars = new char[1024];
//            while ((read = reader.read(chars)) != -1) buffer.append(chars, 0, read);
//            JSONObject jsonObject = new JSONObject(buffer.toString());
//            playlistsContainer = gson.fromJson(buffer.toString(), PlaylistsContainer.class);
//            playlistsContainer = gson.fromJson(jsonObject, PlaylistsContainer.class);
            playlistsContainer = gson.fromJson(reader, PlaylistsContainer.class);
            Log.i("You", "break");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return playlistsContainer;
    }
}
