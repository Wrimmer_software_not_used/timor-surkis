package il.co.razortech.interview.timorsurkis.tuber.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import il.co.razortech.interview.timorsurkis.tuber.R;

/**
 * Created by Wrimmer on 30/11/2015.
 */
public class SimpleCardDivider extends RecyclerView.ItemDecoration {
    private Drawable mDivider;

    public SimpleCardDivider(Context context) {
        mDivider = context.getDrawable(R.drawable.card_divider);
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        int left = parent.getPaddingLeft();
        int right = parent.getWidth() - parent.getPaddingRight();

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int top = child.getBottom() + params.bottomMargin;
            int bottom = top + mDivider.getIntrinsicHeight();

            mDivider.setBounds(left, top, right, bottom);
            mDivider.draw(c);
        }
    }
}
