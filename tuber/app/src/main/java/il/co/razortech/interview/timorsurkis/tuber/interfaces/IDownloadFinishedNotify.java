package il.co.razortech.interview.timorsurkis.tuber.interfaces;

import il.co.razortech.interview.timorsurkis.tuber.baseclasses.data.PlaylistsContainer;

/**
 * Created by Wrimmer on 31/01/2016.
 */
public interface IDownloadFinishedNotify {
    void transferData(PlaylistsContainer container);
}
