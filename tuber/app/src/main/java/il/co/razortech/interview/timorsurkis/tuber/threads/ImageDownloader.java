package il.co.razortech.interview.timorsurkis.tuber.threads;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;

import il.co.razortech.interview.timorsurkis.tuber.interfaces.IImageDownloadFinishedNotify;

/**
 * Created by Wrimmer on 01/02/2016.
 */
public class ImageDownloader extends AsyncTask<String, Void, Bitmap> {
    private IImageDownloadFinishedNotify imageDownloadWaiter;

    public ImageDownloader(IImageDownloadFinishedNotify waiter) {
        imageDownloadWaiter = waiter;
    }

    protected Bitmap doInBackground(String... urls) {
        String url = urls[0];
        Bitmap mIcon = null;
        try {
            InputStream in = new java.net.URL(url).openStream();
            mIcon = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        }
        return mIcon;
    }

    protected void onPostExecute(Bitmap result) {
        imageDownloadWaiter.notifyImageDownloaded(result);
    }
}
