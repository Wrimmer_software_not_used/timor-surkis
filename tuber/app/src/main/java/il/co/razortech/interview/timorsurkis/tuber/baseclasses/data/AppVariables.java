package il.co.razortech.interview.timorsurkis.tuber.baseclasses.data;

/**
 * Created by Wrimmer on 31/01/2016.
 */
public class AppVariables {
    public static final String PLAYLISTS_INTENT_OBJECT_KEY = "Tuber_Playlists_Key";
    public static final String PLAYLISTS_FRAGMENT_OBJECT_KEY = "Tuber_Playlists_Fragment_Key";
}
