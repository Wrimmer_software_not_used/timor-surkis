package il.co.razortech.interview.timorsurkis.tuber.interfaces;

import android.graphics.Bitmap;

/**
 * Created by Wrimmer on 01/02/2016.
 */
public interface IImageDownloadFinishedNotify {
    void notifyImageDownloaded(Bitmap image);
}
