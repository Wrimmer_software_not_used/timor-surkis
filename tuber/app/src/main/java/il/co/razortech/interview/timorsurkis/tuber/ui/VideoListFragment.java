package il.co.razortech.interview.timorsurkis.tuber.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import il.co.razortech.interview.timorsurkis.tuber.R;
import il.co.razortech.interview.timorsurkis.tuber.baseclasses.data.AppVariables;
import il.co.razortech.interview.timorsurkis.tuber.baseclasses.data.PlaylistsContainer;
import il.co.razortech.interview.timorsurkis.tuber.baseclasses.viewholderbaseclasses.ViewHolderParentBaseClassesHolder;

/**
 * Created by Wrimmer on 31/01/2016.
 */
public class VideoListFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_video_list, container, false);

        PlaylistsContainer playlist = (PlaylistsContainer) getArguments().getSerializable(AppVariables.PLAYLISTS_FRAGMENT_OBJECT_KEY);
        ViewHolderParentBaseClassesHolder parentHolder = new ViewHolderParentBaseClassesHolder(playlist);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.playlistsRecyclerView);
        recyclerView.addItemDecoration(new SimpleCardDivider(getActivity()));
        recyclerView.setHasFixedSize(false);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        PlaylistAdapter adapter = new PlaylistAdapter(parentHolder);

        recyclerView.setAdapter(adapter);

        return view;
    }
}
