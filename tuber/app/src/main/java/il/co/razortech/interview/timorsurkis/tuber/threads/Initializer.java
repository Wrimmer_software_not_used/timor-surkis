package il.co.razortech.interview.timorsurkis.tuber.threads;

import android.os.AsyncTask;

import il.co.razortech.interview.timorsurkis.tuber.YoutubeVideosJsonParser;
import il.co.razortech.interview.timorsurkis.tuber.baseclasses.data.PlaylistsContainer;
import il.co.razortech.interview.timorsurkis.tuber.interfaces.IDownloadFinishedNotify;

/**
 * Created by Wrimmer on 31/01/2016.
 */
public class Initializer extends AsyncTask<IDownloadFinishedNotify, Void, PlaylistsContainer> {
    private IDownloadFinishedNotify mNotified;

    @Override
    protected PlaylistsContainer doInBackground(IDownloadFinishedNotify... params) {
        mNotified = params[0];
        return YoutubeVideosJsonParser.downlodListOfYoutubeVideos();
    }

    @Override
    protected void onPostExecute(PlaylistsContainer params) {
        mNotified.transferData(params);
    }
}
