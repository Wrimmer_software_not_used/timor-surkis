package il.co.razortech.interview.timorsurkis.tuber.baseclasses.viewholderbaseclasses;

import java.util.List;

import il.co.razortech.interview.timorsurkis.tuber.baseclasses.data.ListItems;

/**
 * Created by Wrimmer on 01/02/2016.
 */
public class ViewHolderChidBaseClass {
    private List<ListItems> items;

    public ViewHolderChidBaseClass(List<ListItems> items) {
        this.items = items;
    }

    public List<ListItems> getItems() {
        return items;
    }
}
