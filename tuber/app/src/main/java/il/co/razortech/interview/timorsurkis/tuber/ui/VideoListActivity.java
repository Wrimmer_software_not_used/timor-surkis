package il.co.razortech.interview.timorsurkis.tuber.ui;

import android.app.FragmentTransaction;
import android.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import il.co.razortech.interview.timorsurkis.tuber.R;
import il.co.razortech.interview.timorsurkis.tuber.baseclasses.data.AppVariables;
import il.co.razortech.interview.timorsurkis.tuber.baseclasses.data.PlaylistsContainer;

public class VideoListActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_list);

        loadFragment();
    }

    private void loadFragment() {
        PlaylistsContainer playlistsContainer = (PlaylistsContainer) this.getIntent().getSerializableExtra(AppVariables.PLAYLISTS_INTENT_OBJECT_KEY);
        Bundle bundle = new Bundle();

        VideoListFragment fragment = new VideoListFragment();
        fragment.setArguments(bundle);

        bundle.putSerializable(AppVariables.PLAYLISTS_FRAGMENT_OBJECT_KEY, playlistsContainer);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.videoListFragment, fragment);
        fragmentTransaction.commit();
    }
}
