package il.co.razortech.interview.timorsurkis.tuber.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import il.co.razortech.interview.timorsurkis.tuber.R;
import il.co.razortech.interview.timorsurkis.tuber.baseclasses.data.AppVariables;
import il.co.razortech.interview.timorsurkis.tuber.baseclasses.data.PlaylistsContainer;
import il.co.razortech.interview.timorsurkis.tuber.interfaces.IDownloadFinishedNotify;
import il.co.razortech.interview.timorsurkis.tuber.threads.Initializer;

/**
 * Created by Wrimmer on 31/01/2016.
 */
public class SplashScreen extends AppCompatActivity implements IDownloadFinishedNotify {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        new Initializer().execute(this);
    }

    @Override
    public void transferData(PlaylistsContainer container) {
        Intent videoListIntent = new Intent(SplashScreen.this, VideoListActivity.class);
        videoListIntent.putExtra(AppVariables.PLAYLISTS_INTENT_OBJECT_KEY, container);
        startActivity(videoListIntent);
        finish();
    }
}
