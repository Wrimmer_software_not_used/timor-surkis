package il.co.razortech.interview.timorsurkis.tuber.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.util.List;

import il.co.razortech.interview.timorsurkis.tuber.R;
import il.co.razortech.interview.timorsurkis.tuber.baseclasses.data.ListItems;
import il.co.razortech.interview.timorsurkis.tuber.interfaces.IImageDownloadFinishedNotify;
import il.co.razortech.interview.timorsurkis.tuber.threads.ImageDownloader;

/**
 * Created by Wrimmer on 31/01/2016.
 */
public class PlaylistItemsAdapter extends RecyclerView.Adapter<PlaylistItemsAdapter.PlaylistItemsViewHolder> {
    private List<ListItems> mPlaylistItems;

    public PlaylistItemsAdapter(List<ListItems> playlistItems) {
        this.mPlaylistItems = playlistItems;
    }

    @Override
    public PlaylistItemsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.playlist_items_list_item, parent, false);
        return new PlaylistItemsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PlaylistItemsViewHolder holder, int position) {
        holder.setDataInViews(mPlaylistItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mPlaylistItems.size();
    }

    public class PlaylistItemsViewHolder extends RecyclerView.ViewHolder implements IImageDownloadFinishedNotify{
        private Context context;
        private TextView title;
        private ImageView image;
        private boolean didImageDownloadAlready = false;

        public PlaylistItemsViewHolder(View itemView) {
            super(itemView);

            context = itemView.getContext();
            title = (TextView) itemView.findViewById(R.id.playlistItemTitle);
            image = (ImageView) itemView.findViewById(R.id.playlistItemImage);
        }

        public void setDataInViews(final ListItems listItems) {
            title.setText(listItems.getTitle());
            if (!didImageDownloadAlready) {
                new ImageDownloader(this).execute(listItems.getThumb());
            }
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(listItems.getLink()));
                    context.startActivity(browserIntent);
                }
            });
        }

        @Override
        public void notifyImageDownloaded(Bitmap image) {
            didImageDownloadAlready = true;
            this.image.setImageBitmap(image);
        }
    }
}
