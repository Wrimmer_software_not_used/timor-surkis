package il.co.razortech.interview.timorsurkis.tuber.ui;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;

import java.util.List;

import il.co.razortech.interview.timorsurkis.tuber.R;
import il.co.razortech.interview.timorsurkis.tuber.baseclasses.data.ListItems;
import il.co.razortech.interview.timorsurkis.tuber.baseclasses.viewholderbaseclasses.ViewHolderChidBaseClass;
import il.co.razortech.interview.timorsurkis.tuber.baseclasses.viewholderbaseclasses.ViewHolderParentBaseClass;
import il.co.razortech.interview.timorsurkis.tuber.baseclasses.viewholderbaseclasses.ViewHolderParentBaseClassesHolder;

/**
 * Created by Wrimmer on 31/01/2016.
 */
public class PlaylistAdapter extends ExpandableRecyclerAdapter<PlaylistAdapter.PlaylistTitleViewHolder, PlaylistAdapter.PlaylistItemsViewHolder> {

    public PlaylistAdapter(ViewHolderParentBaseClassesHolder parentHolder) {
        super(parentHolder.getParents());
    }

    @Override
    public PlaylistAdapter.PlaylistTitleViewHolder onCreateParentViewHolder(ViewGroup viewGroup) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.playlists_item, viewGroup, false);
        return new PlaylistTitleViewHolder(itemView);
    }

    @Override
    public PlaylistAdapter.PlaylistItemsViewHolder onCreateChildViewHolder(ViewGroup viewGroup) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.playlist_items_list, viewGroup, false);
        return new PlaylistItemsViewHolder(itemView);
    }

    @Override
    public void onBindParentViewHolder(PlaylistTitleViewHolder parentViewHolder, int position, ParentListItem parentListItem) {
        parentViewHolder.setDataInView(((ViewHolderParentBaseClass)parentListItem).getTitle());
    }

    @Override
    public void onBindChildViewHolder(PlaylistItemsViewHolder childViewHolder, int position, Object childListItem) {
        childViewHolder.setDataInView(((ViewHolderChidBaseClass)childListItem).getItems());
    }

    public class PlaylistTitleViewHolder extends ParentViewHolder{
        private TextView playlistTitle;

        public PlaylistTitleViewHolder(View itemView) {
            super(itemView);

            playlistTitle = (TextView) itemView.findViewById(R.id.playlistTitle);
        }

        public void setDataInView(String playlistStringTitle) {
            playlistTitle.setText(playlistStringTitle);
        }
    }

    public class PlaylistItemsViewHolder extends ChildViewHolder{
        private RecyclerView recyclerViewOfList;

        public PlaylistItemsViewHolder(View itemView) {
            super(itemView);

            recyclerViewOfList = (RecyclerView) itemView.findViewById(R.id.playlistListItems);
            recyclerViewOfList.setHasFixedSize(true);
            LinearLayoutManager layoutManager = new org.solovyev.android.views.llm.LinearLayoutManager(itemView.getContext());
            layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerViewOfList.setLayoutManager(layoutManager);
        }

        public void setDataInView(List<ListItems> listItems) {
            PlaylistItemsAdapter adapter = new PlaylistItemsAdapter(listItems);

            recyclerViewOfList.setAdapter(adapter);
        }
    }
}